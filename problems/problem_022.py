# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    gear = []
    if is_workday == True and is_sunny == False:
        gear.append("Umbrella")
    if is_workday == False:
        gear.append("Surfboard")
    if is_workday == True:
        gear.append("Laptop")

    print(gear)

gear_for_day(is_sunny = False, is_workday = True)
