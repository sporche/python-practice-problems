# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    if attendees_list >= (members_list/2):
        print("Over half the members attended!")
    else:
        print("Nobody likes us")

has_quorum(12, 100)
